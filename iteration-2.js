//2.1

// const runTimeOut = async () => {
//     await new Promise((resolve) => {
//         setTimeout(function () {
//             resolve();
//         }, 500);
//     });

//     console.log('Time out!');
// };

// runTimeOut();

//2.2
// const getCharacters = async () => {
//     const result = await fetch('https://rickandmortyapi.com/api/character');
//     const resultToJson = await result.json();
//     return resultToJson;
// }

// getCharacters();