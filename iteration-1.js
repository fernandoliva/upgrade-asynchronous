//1.1
// window.onload = function () {
//     init();
// }

// const init = async () => {
//     console.log('init');
//     const result = await info();
//     console.log(result);

// }

// const info = async () => {
//     const result = await fetch('https://api.agify.io?name=michael');
//     const resultToJson = await result.json();
//     return resultToJson;
// }

//1.2
const baseUrl = 'https://api.nationalize.io?name={}';

let $$button = document.querySelector('button');

const rest = async() => {
    let input = document.querySelector('input');
    let result= await info(input.value);
    console.log(result);
    nameDOM(result);
}

$$button.addEventListener('click',rest);

const info = async(name) => {
    const result = await fetch(`https://api.agify.io?name=${name}`);
    const resultToJson = await result.json();
    return resultToJson;
}

//1.3 & 1.4
const nameDOM = (result) => {
    let $$p = document.createElement('p');
    $$p.textContent = result.name + ' have ' + result.age + ' years.  ';
    let $$button = document.createElement('button');
    $$button.textContent = 'X';

    $$button.addEventListener('click', function deleteItem() {
        this.parentNode.remove();
    });

    document.body.appendChild($$p);
    $$p.appendChild($$button);

}
